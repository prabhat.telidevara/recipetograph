import json
import os
#import glob

#import networkx as nx
#import matplotlib.pyplot as plt

# dictionary of recipes keys to nested recipe calls
NestedRecipes = {}
DTO = {}
Devices = {}
num_devices = 0
Parallel = {}
num_parallel_nodes = 0
ifConditions = {}
num_ifs = 0
elseConditions = {}
num_else = 0

# dictionary to keep track of visited nodes
Recipe_Node = {}
nodes = {}
layers = {}

def recurseNestedRecipe(recipe):

     #print ("Node type " + recipe['partType'])
     #print ("Recipe name " + recipe['name'])

     # populate parallel nodes
     if (recipe['partType'] == "Parallel"):
         parallelnodes(recipe, recipe['name'])

     # populate if condition nodes
     if (recipe['partType'] == "Condition"):
         ifconditional(recipe, recipe['name'])

     # populate else nodes
     if (recipe['partType'] == "Else"):
         elseconditional(recipe, recipe['name'])

     # check if nested recipes exist
     if(recipe['recipes']):

        #print ("Recipe - " + recipe['name'])
        nestedrecipes = recipe['recipes']

        for nestedrecipe in nestedrecipes:
            if recipe['name'] in Recipe_Node:
                Recipe_Node[recipe['name']].append(nestedrecipe['name'])
            else:
                Recipe_Node[recipe['name']] = [nestedrecipe['name']]

            #print(recipe['name'] + " calls " + nestedrecipe['name'])
            recurseNestedRecipe(nestedrecipe)

     # reached end of parent recipe calls
     else:
        #print ("Recipe - " + recipe['name'])
        #print ("no more nested recipes")
        idsToNodes = recipe['idsToNodes']

        # iterate through devices
        for id in idsToNodes:
            recurseidsToNodes(id, recipe['name'])

        return
     return

def elseconditional(recipe, recipename):
     
     idsToNodes = recipe['idsToNodes']

     for id in idsToNodes:
        if(id['node']['type'] == 'Device'):

            if id['node']['nodeData']['instanceName'] in elseConditions:
                elseConditions[id['node']['nodeData']['instanceName']] += 1
            else:
                elseConditions[id['node']['nodeData']['instanceName']] = 1

def ifconditional(recipe, recipename):
     
     idsToNodes = recipe['idsToNodes']

     for id in idsToNodes:

        if recipename in ifConditions:
            ifConditions[recipename] += 1
        else:
            ifConditions[recipename] = 1

def parallelnodes(recipe, recipename):

    idsToNodes = recipe['idsToNodes']

    for id in idsToNodes:

        if id['node']['nodeData']['instanceName'] in Parallel:
            Parallel[id['node']['nodeData']['instanceName']] += 1
        else:
            Parallel[id['node']['nodeData']['instanceName']] = 1

def recurseidsToNodes(id, recipename):

    #print("Node type " + id['node']['type'])
    #print("Inside recipe " + recipename)

    if(id['node']['type'] != 'Special'):
        #print (id['node']['nodeData']['subType'])
        #print (id['node']['nodeData']['instanceName'])
        #print (id['node']['nodeData']['task'])

        if id['node']['nodeData']['instanceName'] in Devices:
            Devices[id['node']['nodeData']['instanceName']] += 1
        else:
            Devices[id['node']['nodeData']['instanceName']] = 1
    else:
        #print (id['node']['nodeData']['subType'])
     return

with open(os.path.join(os.getcwd(), "MAIN.json"), 'r') as f:
    data = json.load(f)

#    for key in data.keys():
#        print (key, len(key))

    recurseNestedRecipe(data)

for key in Parallel.keys():
    num_parallel_nodes += Parallel[key]

DTO["ParallelNodes Count"] = num_parallel_nodes

for key in ifConditions.keys():
    num_ifs += ifConditions[key]

DTO["IF Statements count"] = num_ifs

for key in elseConditions.keys():
    num_else += elseConditions[key]

DTO["Else Statements count"] = num_else

for key in Devices.keys():
    num_devices += Devices[key]

DTO["Devices count"] = num_devices

DTO["Devices"] = Devices

print (DTO)

# create a DTO json
with open(os.path.join('DTO.json'), 'w') as f:
    json.dump(DTO, f)
    f.close()